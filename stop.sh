#!/bin/bash

# abort if any commands return non-zero values
set -e

# shellcheck disable=SC2046
# shellcheck disable=SC2002
export $(grep -v '^#' .env | xargs)

# stop all docker containers
docker-compose -f docker-compose.yml down --rmi all

docker image prune -af

docker network rm "${SHARED_NETWORK}"

docker ps