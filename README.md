# Desc

## Reference to env example and create your own

Please reference `example_primary.env` and `example_replica.env`，and set up user, password, database and container name.

`primary.env`

```env
POSTGRES_USER=<primary user>
POSTGRES_PASSWORD=<primary password>
POSTGRES_DB=<database>
REPLICA_POSTGRES_USER=<replica user>
REPLICA_POSTGRES_PASSWORD=<replica password>
PGDATA=/var/lib/postgresql/data/pgdata
REPLICATE_TO=<replica container name>
REPLICA_NAME=r1
SYNCHRONOUS_COMMIT=off
```

`replica.env`

```env
POSTGRES_USER=<replica user>
POSTGRES_PASSWORD=<replica password>
POSTGRES_DB=<database>
PGDATA=/var/lib/postgresql/data/pgdata
REPLICA_NAME=r1
REPLICATE_FROM=<primary container name>
```

## Read `primary.env` and `replica.env`

```shell
export PRIMARY_FILE=primary.env
export REPLICA_FILE=replica.env
```

## or you can execute `export $(cat .env | xargs)` as well after creating `.env` file.

## If you want to docker files outside of current directory, please setup `CONTEXT` value

## use `wait-for-postgres` shell script to wait for database running

[Ref url](https://docs.docker.com/compose/startup-order/)

You can execute it in `command` of `docker-compose.yml` to wait for database running. Or use the script [wait-for-it](https://github.com/vishnubob/wait-for-it)
or use [dockerize](https://github.com/powerman/dockerize)

```dockerfile
depends_on:
   - <service_name>
command: ["./wait-for-postgres.sh", "<service_name>:5432"]
```
