#!/bin/bash

# abort if any commands return non-zero values
set -e

# shellcheck disable=SC2046
# shellcheck disable=SC2002
export $(grep -v '^#' .env | xargs)

# shellcheck disable=SC2046
if [ -z $(docker network ls --filter name=^"${SHARED_NETWORK}"$ --format="{{ .Name }}") ] ; then
  docker network create "${SHARED_NETWORK}" ;
fi

# stop all docker containers
docker-compose -f docker-compose.yml down --rmi all

docker image prune -af

docker-compose -f docker-compose.yml up -d --build

# display docker containers
docker ps