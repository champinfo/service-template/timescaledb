FROM timescale/timescaledb:latest-pg12

ARG DB_ENV_PATH
ARG SUBMODULE_PATH
ARG INIT_SQL_FILE
ARG PRIMARY_POSTGRES_CONF_FILE
ARG PRIMARY_CONTAINER_NAME
ARG PGDATA

ADD ${DB_ENV_PATH}/primary.env /
RUN export $(grep -v '^#' ${DB_ENV_PATH}/primary.env | xargs)
ADD ${SUBMODULE_PATH}/replication.sh /docker-entrypoint-initdb.d/
RUN chmod +x /docker-entrypoint-initdb.d/replication.sh
ADD ${INIT_SQL_FILE} /docker-entrypoint-initdb.d/
ADD ${PRIMARY_POSTGRES_CONF_FILE} /