FROM timescale/timescaledb:latest-pg12

ARG DB_ENV_PATH
ARG SUBMODULE_PATH
ARG PRIMARY_POSTGRES_CONF_FILE
ARG REPLICA_CONTAINER_NAME
ARG PGDATA

ADD ${DB_ENV_PATH}/replica.env /
RUN export $(grep -v '^#' ${DB_ENV_PATH}/replica.env | xargs)
ADD ${SUBMODULE_PATH}/replication.sh /docker-entrypoint-initdb.d/
RUN chmod +x /docker-entrypoint-initdb.d/replication.sh
ADD ${PRIMARY_POSTGRES_CONF_FILE} /